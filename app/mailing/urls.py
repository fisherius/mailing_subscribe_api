from rest_framework.routers import DefaultRouter
from django.urls import path, include
from mailing.views import ClientViewSet, MailingViewSet, MessagingViewSet, TestSchedViewSet

router = DefaultRouter()

router.register('client', ClientViewSet, basename='ClientVieSet')
router.register('mailing', MailingViewSet, basename='MailingViewSet')

#  по заданию это не нужно
# router.register('message', MessagingViewSet, basename='MessagingViewSet')

urlpatterns = [
    path('', include(router.urls)),
    path('test', TestSchedViewSet.as_view()),
    ]
