from rest_framework import serializers
from mailing.models import Message, Mailing, Client
from django.db.models import Q


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            'id',
            'send_time',
            'status',
            'mailing',
            'client',
        )


class MailingSerializer(serializers.ModelSerializer):
    send_stats = serializers.ReadOnlyField()

    class Meta:
        model = Mailing
        fields = (
            'id',
            'start_time',
            'message_text',
            'filter_who',
            'end_time',
            'send_stats',
        )

    @staticmethod
    def create_messages(mailing):
        filters = mailing.filter_who.split(',')
        print(filters)
        target_client = Client.objects.filter(Q(tage__in=filters) | Q(code__in=filters))
        for client in target_client:
            Message.objects.create(mailing=mailing, client=client, status=Message.QUEUED)

    def create(self, validated_data):

        mailing = super().create(validated_data)
        self.create_messages(mailing)

        return mailing

    def update(self, instance, validated_data):
        msg_update = instance.filter_who != validated_data['filter_who']
        upd_instance = super().update(instance, validated_data)

        if msg_update:
            Message.objects.filter(mailing=upd_instance).delete()
            self.create_messages(upd_instance)

        return upd_instance







