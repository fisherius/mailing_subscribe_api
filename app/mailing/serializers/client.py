from rest_framework import serializers
from mailing.models import Client


class ClientSerializer(serializers.ModelSerializer):
    code = serializers.ReadOnlyField()

    class Meta:
        model = Client
        fields = (
            'id',
            'phone',
            'code',
            'timezone',
        )
