from django.conf import settings
from celery import shared_task
import requests
from mailing.models import Message
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone


@shared_task
def send_msg(msg_id):
    try:
        message_obj = Message.objects.get(pk=msg_id)
    except ObjectDoesNotExist:
        # Может возникнуть ситуация когда объект сообщения был удален, на пример во время обновления рассылки
        print(f'{msg_id = } {ObjectDoesNotExist}')
        return False
    url = f'https://probe.fbrq.cloud/v1/send/{message_obj.id}'
    message_text = message_obj.mailing.message_text
    phone = int(message_obj.client.phone)
    head = {
        'Authorization': 'Bearer ' + settings.SMS_AUTH_TOKEN,
        'Content-Type': 'application/json',
        'accept': 'application/json',
    }
    data = {
        'id': message_obj.id,
        'phone': phone,
        'text': message_text,
    }
    try:
        # отправка сообщения через другой сервис
        response = requests.post(url, json=data, headers=head)

        # если все хорошо то помечаем сообщение как доставленное
        if response.status_code == 200:
            resp_data = response.json()
            if resp_data['code'] == 0:
                message_obj.status = Message.DELIVERED
                message_obj.send_time = timezone.now()
                message_obj.save()
                return True
    except Exception as e:
        print(e)
    # в случае любого неудачного случая отправки ставим статус о "провале".
    message_obj.status = Message.FAILED
    message_obj.save()
    return False


@shared_task(name='scheduler_mailing')
def scheduler_mailing():
    now = timezone.now()

    # Все активные сообщения которые надо доставить подходящие по промежутку времени
    all_messages = Message.objects.filter(mailing__start_time__lte=now, mailing__end_time__gte=now).\
        exclude(status__in=['S', 'D'])

    # создаем задания на отправку сообщений
    for msg in all_messages:
        send_msg.delay(msg.id)
        msg.status = Message.SEND
        msg.save()








