from django.db import models
from django.core.exceptions import ValidationError
import pytz


#  проверка телефона на валидность
def validate_phone(phone):
    if len(phone) != 11:
        raise ValidationError("Длинна номера не соответствует требуемому")
    if phone[0] != '7':
        raise ValidationError("Номер должен начинаться с 7")
    if phone.isdigit():
        return phone
    else:
        raise ValidationError("Но мер должне состоять из цифр")


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone = models.CharField(max_length=11, verbose_name='номер телефона', validators=[validate_phone], unique=True)
    code = models.CharField(max_length=4, verbose_name='код мобильного оператора')
    tage = models.CharField(max_length=32, verbose_name='тег')
    timezone = models.CharField(verbose_name='часовой пояс', max_length=32, choices=TIMEZONES,  default='UTC')

    def save(self, *args, **kwargs):
        self.code = self.phone[1:4]
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.phone[0]} ({self.phone[1:4]}) {self.phone[4:]}, {self.tage}, {self.timezone}'
