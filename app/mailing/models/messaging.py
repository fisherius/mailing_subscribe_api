from django.db import models
from mailing.models import Client


class Message(models.Model):
    QUEUED = 'Q'
    SEND = 'S'
    DELIVERED = 'D'
    FAILED = 'F'
    EXPIRED = 'E'
    UNKNOWN = 'U'
    REJECTED = 'R'

    STATUS = (
        (QUEUED, 'Queued'),
        (SEND, 'Sent'),
        (DELIVERED, 'Delivered'),
        (FAILED, 'Expired'),
        (EXPIRED, 'Rejected'),
        (UNKNOWN, 'Unknown'),
        (REJECTED, 'Failed'),
    )
    send_time = models.DateTimeField('Время отправки', blank=True, null=True, default=None)
    status = models.CharField(verbose_name='Текущий статус', max_length=1, choices=STATUS, default=QUEUED)
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE, verbose_name='Рассылка', related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Клиент', related_name='messages')

    def __str__(self):
        return f'{self.mailing}, {self.client}, {self.status}, {self.send_time}'


class Mailing(models.Model):
    start_time = models.DateTimeField(verbose_name='Запуск рассылки')
    message_text = models.TextField(verbose_name='Текст сообщения')
    filter_who = models.CharField(verbose_name='Фильтр свойств клиентов', max_length=128)
    end_time = models.DateTimeField(verbose_name='Запуск рассылки')

    @property
    def send_stats(self):
        msgs = self.messages.all()
        result = {status[1]: msgs.filter(status=status[0]).count() for status in Message.STATUS}
        result['TOTAL'] = msgs.count()
        return result

    def __str__(self):
        return f'"{self.message_text[:100]}", [{self.filter_who}]'

