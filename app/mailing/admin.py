from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser, Message, Mailing, Client


class CustomUserAdmin(UserAdmin):
    model = CustomUser


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Mailing)
