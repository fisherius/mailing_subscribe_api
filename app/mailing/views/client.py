from rest_framework import viewsets, mixins
from mailing.models import Client
from mailing.serializers import ClientSerializer
from rest_framework.permissions import IsAdminUser


class ClientViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin, mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    permission_classes = [IsAdminUser]
