from rest_framework import viewsets, mixins, views
from rest_framework.permissions import IsAdminUser
from mailing.serializers import MailingSerializer, MessageSerializer
from mailing.models import Mailing, Message
from mailing.tasks import scheduler_mailing
from rest_framework.response import Response


class TestSchedViewSet(views.APIView):
    def post(self, request):
        task = scheduler_mailing.delay()
        return Response({'id': task.id}, status=200)


class MailingViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                    mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()
    permission_classes = [IsAdminUser]


class MessagingViewSet(mixins.RetrieveModelMixin,
                    mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    permission_classes = [IsAdminUser]

    # serializers = {
    #     'list': ListOrderSerializer,
    #     'create': CreateOrderSerializer,
    #     'retrieve': RetrieveOrderSerializer,
    # }
    # permission_classes = [permissions.IsAuthenticated]
    #
    # def get_serializer_class(self):
    #     return self.serializers[self.action]
    #
    # def get_queryset(self):
    #     if self.request.user.is_authenticated:
    #         return self.queryset.filter(user=self.request.user)
    #
    # def get_object(self):
    #     try:
    #         return self.queryset.get(user=self.request.user, pk=self.kwargs['pk'])
    #     except ObjectDoesNotExist:
    #         raise Http404