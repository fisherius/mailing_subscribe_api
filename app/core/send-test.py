import requests


if __name__ == '__main__':
    auth_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDE1MjI5NTAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImZpc2hlcmJ5In0.7uotky3dYmzgRBhTT2iotUx598CbQTZW5BQG9h5pIvM'
    url = 'https://probe.fbrq.cloud/v1/send/'

    head = {
        'Authorization': 'Bearer ' + auth_token,
        # 'Content-Type': 'application/json',
        # 'accept': 'application/json',
    }

    data = {
        'id': 100,
        'phone': 799999999999,
        'text': 'testmsg',
    }

    response = requests.post(url+'100', json=data, headers=head)
    print(response)
    print(response.status_code)
    if response.status_code == 200:
        print(response.json())

