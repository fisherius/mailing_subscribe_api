import random
import pytz
import string
from mailing.models import Client

TAGS = [''.join([random.choice(string.ascii_lowercase) for _ in range(random.randint(3, 8))]) for _ in range(20)]


def rnd_phone(template='7xxxxxxxxxx'):
    return ''.join([random.choice(string.digits) if d == 'x' else d for d in template.lower()])


def rnd_tag(choice=True):
    if choice:
        return random.choice(TAGS)
    else:
        return ''.join([random.choice(string.ascii_lowercase) for _ in range(random.randint(3, 8))])


def rnd_clients(count):
    return [{'phone': rnd_phone(), 'tage': rnd_tag(), 'timezone': random.choice(pytz.all_timezones)} for i in range(count)]


def clients_to_db(clients):
    return [Client.objects.create(**c) for c in clients]


def add_rnd_clients(count):
    clients = clients_to_db(rnd_clients(count))
    print(clients)
